<%-- 
    Document   : registro
    Created on : 03-abr-2015, 20:22:43
    Author     : Álvaro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>superTienda - Registro</title>
        <link rel="stylesheet" href="static/css/main.css" type="text/css"/>
    </head>
    <body>
        <main>
            <h1>Registro</h1>
            <form name="registro" action="" method="post">
                <input type="hidden" name="op" value="registrar">
                <p>Nombre: <input type="text" pattern="^([ \u00c0-\u01ffa-zA-Z'\-])+$" name="nombre" required/></p>
                <p>Apellidos: <input type="text" pattern="^([ \u00c0-\u01ffa-zA-Z'\-])+$" name="apellidos" required/></p>
                <p>Contraseña: <input type="password" pattern="^[a-zA-Z0-9]{4,}$" name="pass" required/></p>
                <p>Repetir contraseña: <input type="password" pattern="^[a-zA-Z0-9]{4,}$" name="repe" required/></p>
                <p>Correo electrónico: <input type="email" name="email" required/></p>
                <input type="submit" value="Enviar"/>
            </form>
            <form name="volver" method="post" action="">
                <input type="hidden" name="op" value=""> 
                <input type="submit" value="Volver">
            </form>
        </main>
    </body>
</html>
