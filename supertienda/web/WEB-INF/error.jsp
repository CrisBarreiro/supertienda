<%-- 
    Document   : error
    Created on : 03-abr-2015, 1:00:20
    Author     : Cristina
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error</title>
        <link rel="stylesheet" href="static/css/main.css" type="text/css"/>
    </head>
    <body>
        <main>
            <h1>Error</h1>
            ${requestScope.mensaje}
            <form name="volver" method="post" action="">
                <input type="hidden" name="op" value=""> 
                <input type="submit" value="Volver">
            </form>
        </main>
    </body>

</html>
