<%-- 
    Document   : buscar
    Created on : 03-abr-2015, 11:56:26
    Author     : Álvaro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf8">
        <title>superTienda - Búsqueda</title>
        <link rel="stylesheet" href="static/css/main.css" type="text/css"/>
    </head>
    <body>
        <main>
            <h1>Búsqueda</h1>
            <p>Introduce los campos pertinentes y pulsa "Buscar"</p>
            <form name="busqueda" method="post" action="">
                <input type="hidden" name="op" value="do_search"/>
                <p>Título: <input type="text" name="titulo"/></p>
                <p>Autor: <input type="text" name="autor"/></p>
                <p>País: <input type="text" name="pais"</p>
                <p>Precio (máximo): <input type="number" min="0" step="any" name="precio" /></p>
                <p><input type="submit" value="Buscar"></p>
            </form>
            <form name="volver" method="post" action="">
                <input type="hidden" name="op" value=""> 
                <input type="submit" value="Volver">
            </form>
        </main>
    </body>
</html>
