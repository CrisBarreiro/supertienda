<%-- 
    Document   : editarUsuario
    Created on : 03-abr-2015, 21:05:09
    Author     : Cristina
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Modificar contraseña</title>
        <link rel="stylesheet" href="static/css/main.css" type="text/css"/>
    </head>
    <body>
        <main>
            <h1>Modificar contraseña</h1>
            <p>Usuario: ${user}</p>
            <form name="modificarContraseña" method="post" action="" onsubmit="checkPass();">
                <p>Introduzca la contraseña: <input type="password" name="pass" placeholder="contraseña" pattern="^[a-zA-Z0-9]{4,}$" required/></p>
                <p>Repita la contraseña: <input type="password" name="pass2" placeholder="contraseña" pattern="^[a-zA-Z0-9]{4,}$"/></p>
                <input type="hidden" name="op" value="nuevaContrasena"/>
                <input type="hidden" name="user" value="${user}"/>
                <input type="submit" value="Modificar contraseña"/>
            </form>
            <p>
            <form name="volver" method="post" action="">
                <input type="hidden" name="op" value=""> <%-- triggers default action --%>
                <input type="submit" value="Volver">
            </form>
        </p>
    </main>
</body>

</html>
