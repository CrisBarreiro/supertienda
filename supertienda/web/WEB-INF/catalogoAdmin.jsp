<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="es_ES"/>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf8">
        <title>SuperTienda - Catálogo</title>
        <link rel="stylesheet" href="static/css/main.css" type="text/css"/>
    </head>
    <body>
        <main>
            <h1>Catálogo de productos</h1>
            <table>
                <tr>
                    <th>Código</th>
                    <th>Título</th>
                    <th>Intérprete</th>
                    <th>País</th>
                    <th>Precio</th>
                    <th>Stock</th>
                </tr>

                <c:forEach var="cd" items="${listaCDs.CDs}">
                    <tr>
                        <td><fmt:formatNumber pattern="0000" value="${cd.codigo}"/></td>
                        <td>${cd.titulo}</td>
                        <td>${cd.interprete}</td>
                        <td>${cd.pais}</td>
                        <td><fmt:formatNumber type="currency" value="${cd.precio}"/></td>
                        <td>
                            <form name="modCan-${cd.codigo}" method="post" action="">
                                <input style="width:50px" type="number" name="stock" min="0"value="${cd.stock}"/>
                                <input type="hidden" name="op" value="modificarStock"/>
                                <input type="hidden" name="cd" value="${cd.codigo}"/>
                                <input type="submit" value="Modificar"/>
                            </form>
                        </td>

                    </tr>
                </c:forEach>


            </table>

            <p>
            <div id="pages">Página <b>${listaCDs.numeroPagina}</b> de ${totalPaginas}
                <form name="siguiente" method="post" action="">
                    <input type="hidden" name="op" value="chpage_cat"/>
                    <input type="hidden" name="pag" value="${listaCDs.numeroPagina +1}"/>
                    <input type="submit" value="Siguiente"/>
                </form>
                <form name="anterior" method="post" action="">
                    <input type="hidden" name="op" value="chpage_cat"/>
                    <input type="hidden" name="pag" value="${listaCDs.numeroPagina -1}"/>
                    <input type="submit" value="Anterior"/>
                </form>
            </div>
        </p>

        <div id="menu"> <form name="srch" method="post" action="">
                <input type="hidden" name="op" value="busqueda"/>
                <input type="submit" value="Buscar"/>
            </form>


            <form name="volv" method="post" action="">
                <input type="hidden" name="op" value="volv"/>
                <input type="submit" value="Volver"/>
            </form></div>

    </main>
</body>
</html>
