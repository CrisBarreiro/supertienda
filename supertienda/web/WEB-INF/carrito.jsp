<%-- 
    Document   : carrito
    Created on : 02-abr-2015, 21:36:09
    Author     : Álvaro
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<fmt:setLocale value="es_ES"/>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf8">
        <title>superTienda - Carrito</title>
        <link rel="stylesheet" href="static/css/main.css" type="text/css"/>
    </head>
    <body>
        <main>
            <h1>Carrito de la compra</h1>
            <c:choose>
                <c:when test="${carrito!=null && !carrito.lineasCompra.isEmpty()}">
                    <form name="cant" method="post" action="">
                        <input type="hidden" name="op" value="change_number"/>
                        <table>
                            <tr>
                                <th>Producto</th>
                                <th>PVP</th>
                                <th>Cantidad</th>
                                <th>Total</th>
                            </tr>

                            <c:forEach var="linea" items="${carrito.lineasCompra}">
                                <c:set var="cd" value="${linea.cd}"/>
                                <tr>


                                    <td>${cd.titulo}</td>
                                    <td><fmt:formatNumber type="currency" value="${cd.precio}"/></td>
                                    <td><input name="${cd.codigo}" type="number" min="0" max="${cd.stock}" value="${linea.cantidad}"></td>
                                    <td><fmt:formatNumber type="currency" value="${cd.precio * linea.cantidad}"/></td>

                                </tr>
                            </c:forEach>

                        </table>
                        <input type="submit" value="Guardar cambios"/>
                    </form>
                    <p>
                    <form name="comprar" method="post" action="">
                        <input type="hidden" name="op" value="comprar">
                        <input type="submit" value="Comprar">
                    </form>
                </p>
                </c:when>
                <c:otherwise>
                    ¡Tu carrito de la compra está vacío! ¿Por qué no añades algún producto?
                </c:otherwise>
            </c:choose>
            <p>
            <form name="volver" method="post" action="">
                <input type="hidden" name="op" value=""> <%-- triggers default action --%>
                <input type="submit" value="Volver">
            </form>
        </p>
    </main>
</body>
</html>
