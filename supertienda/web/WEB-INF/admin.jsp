<%-- 
    Document   : admin
    Created on : 04-abr-2015, 10:35:07
    Author     : Álvaro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf8">
        <title>Zona de administrador</title>
        <link rel="stylesheet" href="static/css/main.css" type="text/css"/>
    </head>
    <body>
        <main>
            <h1>Administración</h1>
            <div id="menu">            <form name="listarClientes" method="post" action="">
                    <input type="hidden" name="op" value="listarClientes"/>
                    <input type="submit" value="Listar clientes"/>
                </form>
                <form name="verCatalogo" method="post" action="">
                    <input type="hidden" name="op" value="catalogo"/>
                    <input type="submit" value="Ver catálogo"/>
                </form>
                <form name="nuevoCD" method="post" action="">
                    <input type="hidden" name="op" value="nuevoCD"/>
                    <input type="submit" value="Nuevo CD"/>
                </form>
                <form name="exit" method="post" action="">
                    <input type="hidden" name="op" value="salir"/>
                    <input type="submit" value="Salir"/>
                </form></div>
        </main>
    </body>
</html>
