<%-- 
    Document   : nuevoCD
    Created on : 04-abr-2015, 11:47:02
    Author     : Álvaro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Introducir nuevo CD </title>
        <link rel="stylesheet" href="static/css/main.css" type="text/css"/>
    </head>
    <body>
        <main>
            <h1>Nuevo CD</h1>
            <form name="newcd" method="post" action="">
                <input type="hidden" name="op" value="do_newcd"/>
                <p>Título: <input type="text" name="titulo" required/></p>
                <p>Intérprete <input type="text" name="interprete" required/></p>
                <p>País: <input type="text" name="pais" required/></p>
                <p>Precio: <input type="number" min="0" step="any" name="precio" required/></p>
                <p>Stock: <input type="number" min="0" step="1" name="stock" required/></p>
                <p><input type="submit" value="Guardar"/>
                <p>El código será generado automáticamente</p>
            </form>
            <form name="volver" method="post" action="">
                <input type="hidden" name="op" value=""> 
                <input type="submit" value="Volver">
            </form>
        </main>
    </body>
</html>
