<%-- 
    Document   : resultados_busqueda
    Created on : 03-abr-2015, 12:27:52
    Author     : Ãlvaro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="es_ES"/>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf8">
        <title>SuperTienda - Búsqueda</title>
        <link rel="stylesheet" href="static/css/main.css" type="text/css"/>
    </head>
    <body>
        <main>
            <form name="catalogo" action="" method="post">
                <input type="hidden" name="op" value="add_product"/>
                <h1>Resultados de la búsqueda</h1>
                <table>
                    <tr>
                        <th>Código</th>
                        <th>Título</th>
                        <th>Intérprete</th>
                        <th>País</th>
                        <th>Precio</th>
                    </tr>

                    <c:forEach var="cd" items="${listaCDs.CDs}">
                        <tr>
                            <td><fmt:formatNumber pattern="0000" value="${cd.codigo}"/></td>
                            <td>${cd.titulo}</td>
                            <td>${cd.interprete}</td>
                            <td>${cd.pais}</td>
                            <td><fmt:formatNumber type="currency" value="${cd.precio}"/></td>
                            <td><input type="checkbox" name="items" value="${cd.codigo}" ${cd.stock>0 ? "" : "disabled"} /></td>
                        </tr>
                    </c:forEach>


                </table>
                <input type="submit" value="Añadir al carrito"/>
            </form>
            <p>
            <div id="pages">Página <b>${listaCDs.numeroPagina}</b> de ${totalPaginas}
                <form name="siguiente" method="post" action="">
                    <input type="hidden" name="op" value="chpage_sch"/>
                    <input type="hidden" name="pag" value="${listaCDs.numeroPagina +1}"/>
                    <input type="submit" value="Siguiente"/>
                </form>
                <form name="anterior" method="post" action="">
                    <input type="hidden" name="op" value="chpage_sch"/>
                    <input type="hidden" name="pag" value="${listaCDs.numeroPagina -1}"/>
                    <input type="submit" value="Anterior"/>
                </form>
            </div>
        </p>
        <p>
        <form name="volver" method="post" action="">
            <input type="hidden" name="op" value=""> 
            <input type="submit" value="Volver">
        </form>
    </p>
</main>
</body>
</html>

