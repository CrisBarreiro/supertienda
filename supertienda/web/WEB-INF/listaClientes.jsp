<%-- 
    Document   : listaClientes
    Created on : 03-abr-2015, 19:52:05
    Author     : Cristina
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista de Clientes</title>
        <link rel="stylesheet" href="static/css/main.css" type="text/css"/>

    </head>
    <body>
        <main>
            <h1>Lista de Clientes</h1>

            <table>
                <tr>
                    <th>Nombre de usuario</th>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>Password</th>
                    <th>Vip</th>
                </tr>
                <c:forEach var="cliente" items="${listaClientes.clientes}">
                    <tr>
                        <td>${cliente.mail}</td>
                        <td>${cliente.nombre}</td>
                        <td>${cliente.apellidos}</td>
                        <td>${cliente.password}</td>
                        <td>${cliente.vip}</td>
                        <td>
                            <form name="eliminarCliente-${cliente.mail}" action="" method="post">
                                <input type="hidden" name="op" value="eliminarUsuario"/>
                                <input type="hidden" name="user" value="${cliente.mail}"/>
                                <input type="submit" value="Eliminar"/>
                            </form>
                        </td>
                        <td><form name="modificar-${cliente.mail}" method="post" action="">
                                <input type="hidden" name="op" value="modificarContrasena"/>
                                <input type="hidden" name="user" value="${cliente.mail}"/>
                                <input type="submit" value="Modificar contraseña"/>
                            </form>
                        </td>

                    </tr>
                </c:forEach>
            </table>

            <p>
            <div id="pages">Página <b>${listaClientes.numeroPagina}</b> de ${totalPaginas}
                <form name="anterior" method="post" action="">
                    <input type="hidden" name="op" value="chpage_usr"/>
                    <input type="hidden" name="pag" value="${listaClientes.numeroPagina -1}"/>
                    <input type="submit" value="Anterior"/>
                </form>
                <form name="siguiente" method="post" action="">
                    <input type="hidden" name="op" value="chpage_usr"/>
                    <input type="hidden" name="pag" value="${listaClientes.numeroPagina +1}"/>
                    <input type="submit" value="Siguiente"/>
                </form>
            </div>
        </p>

        <form name="exit" method="post" action="">
            <input type="hidden" name="op" value="a"/>
            <input type="submit" value="Volver"/>
        </form>
    </main>
</body>
</html>
