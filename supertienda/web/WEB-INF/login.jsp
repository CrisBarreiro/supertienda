<%-- 
    Document   : login
    Created on : 02-abr-2015, 17:31:23
    Author     : Cristina
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <link rel="stylesheet" href="static/css/main.css" type="text/css"/>

    </head>
    <body>
        <main>
            <h1>Login</h1>
            <form name="login" action="" method="post">
                <p>Correo electrónico<input type="text" placeholder="correo electrónico" name="mail"/></p>
                <p>Contraseña<input type="password" placeholder="contraseña" name="pass"/></p>
                <input type="hidden" name="op" value="entrar"/>
                <p><input type="submit" value="Entrar"/></p>
            </form>
            <form name="volver" method="post" action="">
                <input type="hidden" name="op" value=""> 
                <input type="submit" value="Volver">
            </form>
        </main>
    </body>
</html>

