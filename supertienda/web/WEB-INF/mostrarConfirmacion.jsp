<%-- 
    Document   : mostrarConfirmacion
    Created on : 03-abr-2015, 15:59:11
    Author     : Cristina
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="es_ES"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Confirmar Compra</title>
        <link rel="stylesheet" href="static/css/main.css" type="text/css"/>
    </head>
    <body>
        <main>
            <h1>Confirmar Compra</h1>
            <p>Valor del carrito: <b><fmt:formatNumber type="currency" value="${carrito.precioTotal}"/></b></p>
            <c:if test="${carrito.descuento>0}">
                <p>Por ser usuario VIP, se te ha concedido un descuento de <fmt:formatNumber type="currency" value="${carrito.descuento}"/></p>
                <p>Precio final de la compra: <b><fmt:formatNumber type="currency" value="${carrito.precioTotal - carrito.descuento}"/></b></p>
            </c:if>
            <form name="confirmar" method="post" action="">
                <input type="hidden" name="op" value="confirm"/>
                <input type="submit" value="Confirmar"/>
            </form>
            <form name="volver" method="post" action="">
                <input type="hidden" name="op" value="carrito">
                <input type="submit" value="Volver">
            </form>
        </main>
    </body>
</html>
