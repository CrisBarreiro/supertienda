
<%-- 
    Document   : index
    Created on : 29-mar-2015, 22:46:11
    Author     : Media
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="es_ES"/>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf8">
        <title>SuperTienda - Catálogo</title>
        <link rel="stylesheet" href="static/css/main.css" type="text/css"/>
    </head>
    <body>
        <main>
            <form name="catalogo" action="" method="post">
                <input type="hidden" name="op" value="add_product"/>
                <h1>Catálogo de productos</h1>
                <table>
                    <tr>
                        <th>Código</th>
                        <th>Título</th>
                        <th>Intérprete</th>
                        <th>País</th>
                        <th>Precio</th>
                    </tr>

                    <c:forEach var="cd" items="${listaCDs.CDs}">
                        <tr>
                            <td><fmt:formatNumber pattern="0000" value="${cd.codigo}"/></td>
                            <td>${cd.titulo}</td>
                            <td>${cd.interprete}</td>
                            <td>${cd.pais}</td>
                            <td><fmt:formatNumber type="currency" value="${cd.precio}"/></td>
                            <td><input type="checkbox" name="items" value="${cd.codigo}" ${cd.stock>0 ? "" : "disabled"} /></td>
                        </tr>
                    </c:forEach>


                </table>
                <input type="submit" value="Añadir al carrito"/>
            </form>
            <p>
            <div id="pages">Página <b>${listaCDs.numeroPagina}</b> de ${totalPaginas}
                <form name="siguiente" method="post" action="">
                    <input type="hidden" name="op" value="chpage_cat"/>
                    <input type="hidden" name="pag" value="${listaCDs.numeroPagina +1}"/>
                    <input type="submit" value="Siguiente"/>
                </form>
                <form name="anterior" method="post" action="">
                    <input type="hidden" name="op" value="chpage_cat"/>
                    <input type="hidden" name="pag" value="${listaCDs.numeroPagina -1}"/>
                    <input type="submit" value="Anterior"/>
                </form>
            </div>
        </p>
        <p>
        <div id="menu"> <c:choose>
                <c:when test="${sessionScope.user == null}">
                    <form name="log" method="post" action="">
                        <input type="hidden" name="op" value="login"/>
                        <input type="submit" value="Login"/>
                    </form>
                    <form name="log" method="post" action="">
                        <input type="hidden" name="op" value="registro"/>
                        <input type="submit" value="Registro"/>
                    </form>
                </c:when>
                <c:otherwise>
                    <form name="exit" method="post" action="">
                        <input type="hidden" name="op" value="salir"/> (${sessionScope.user.mail})
                        <input type="submit" value="Salir"/>
                    </form>
                </c:otherwise>
            </c:choose>
            <form name="car" method="post" action="">
                <input type="hidden" name="op" value="carrito"/>
                <input type="submit" value="Ver carrito"/>
            </form>
            <form name="srch" method="post" action="">
                <input type="hidden" name="op" value="busqueda"/>
                <input type="submit" value="Buscar"/>
            </form>
        </div>
    </p>
</main>
</body>
</html>
