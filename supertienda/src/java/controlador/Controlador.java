package controlador;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.Admin;
import modelo.CD;
import modelo.Carrito;
import modelo.Cliente;
import modelo.Usuario;

public class Controlador extends HttpServlet {

    public Controlador() {
    }

    public void procesador(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //parameter initialization block
        HttpSession ses = request.getSession(true);
        Carrito k = (Carrito) ses.getAttribute("carrito");
        if (k == null) {
            k = new Carrito();
        }
        Usuario u = (Usuario) ses.getAttribute("user");
        Integer page = (Integer) ses.getAttribute("page");
        if (page == null) {
            page = 1;
        }
        //end block

        String op = (String) request.getParameter("op");
        String redir = "";
        Carrito temp = null;
        if (op != null) {
            switch (op) {

                case "catalogo":
                    page = 1;
                    redir = "WEB-INF/catalogoAdmin.jsp";
                    break;

                case "login":
                    redir = "WEB-INF/login.jsp";
                    break;

                case "carrito":
                    redir = "WEB-INF/carrito.jsp";
                    break;

                case "entrar":
                    Usuario usr = new UserHelper().login(request);
                    if (usr == null) {
                        request.setAttribute("mensaje", "Datos de login incorrectos");
                        redir = "WEB-INF/error.jsp";
                    } else {
                        ses.setAttribute("user", usr.isAdmin() ? (Admin) usr : (Cliente) usr);
                        if (usr.isAdmin()) {
                            redir = "WEB-INF/admin.jsp";
                        } else {
                            if (ses.getAttribute("carrito") != null){
                                redir = "WEB-INF/carrito.jsp";
                            }
                            else{
                                redir = "WEB-INF/catalogo.jsp";
                            }
                        }
                    }

                    break;

                case "salir":
                    ses.removeAttribute("user");
                    ses.removeAttribute("admin");
                    if (ses.getAttribute("carrito") != null){
                        ses.removeAttribute("carrito");
                    }
                    redir = "WEB-INF/catalogo.jsp";
                    break;

                case "busqueda":
                    redir = "WEB-INF/buscar.jsp";
                    break;

                case "do_search":
                    page = 1;
                    CDPageHelper cd = new CDPageHelper();
                    CD src=cd.genCD(request);
                    request.setAttribute("listaCDs", cd.getSearchPage(src, 1));
                    request.setAttribute("totalPaginas", cd.getPagesTotal(src));
                    ses.setAttribute("saved_search", src);
                    
                    System.out.println(ses.getAttribute("saved_search"));
                    if (u == null || !u.isAdmin()) {
                        redir = "WEB-INF/resultados_busqueda.jsp";
                    } else {
                        redir = "WEB-INF/resultados_busqueda_admin.jsp";
                    }
                    break;

                case "chpage_sch":
                    page = Integer.parseInt(request.getParameter("pag"));
                    CDPageHelper cd1 = new CDPageHelper();
                    CD src1=(CD) ses.getAttribute("saved_search");
                    request.setAttribute("listaCDs", cd1.getSearchPage(src1, page));
                    request.setAttribute("totalPaginas", cd1.getPagesTotal(src1));
                    if (u == null || !u.isAdmin()) {
                        redir = "WEB-INF/resultados_busqueda.jsp";
                    } else {
                        redir = "WEB-INF/resultados_busqueda_admin.jsp";
                    }
                    break;

                case "change_number":
                    temp = new CompraHelper().modificarCarrito(request, k);
                    ses.setAttribute("carrito", temp);
                    redir = "WEB-INF/carrito.jsp";
                    break;

                case "add_product":
                    temp = new CompraHelper().añadirProducto(request, k);
                    ses.setAttribute("carrito", temp);
                    redir = "WEB-INF/catalogo.jsp";
                    break;

                case "comprar":
                    if (ses.getAttribute("user") != null){
                        temp = new CompraHelper().aplicarDescuento(k, (Cliente) ses.getAttribute("user"));
                        ses.setAttribute("carrito", temp);
                        redir = "WEB-INF/mostrarConfirmacion.jsp";
                    }
                    else {
                        redir = "WEB-INF/login.jsp";
                    }
                    
                    break;

                case "confirm":
                    Cliente a = new CompraHelper().realizarCompra(k, (Cliente) ses.getAttribute("user"));
                    ses.setAttribute("user", a);
                    ses.removeAttribute("carrito");
                    redir = "WEB-INF/catalogo.jsp";
                    page = 1;
                    break;

                case "listarClientes":
                    redir = "WEB-INF/listaClientes.jsp";
                    page = 1;
                    break;

                case "eliminarUsuario":
                    new UserHelper().eliminarCliente(request);
                    redir = "WEB-INF/listaClientes.jsp";
                    break;

                case "modificarContrasena":
                    redir = "WEB-INF/editarUsuario.jsp";
                    request.setAttribute("user", request.getParameter("user"));
                    break;

                case "registro":
                    redir = "WEB-INF/registro.jsp";
                    break;

                case "registrar":
                    if (new UserHelper().registrar(request)) {
                        redir = "WEB-INF/exito.jsp";
                        request.setAttribute("mensaje", "Se ha registrado al usuario <b>" + request.getParameter("email") + "</b> con éxito");
                    } else {
                        redir = "WEB-INF/error.jsp";
                        request.setAttribute("mensaje", "Se ha producido un error. Puede ser que se hayan introducido parámetros incorrectos");
                    }
                    break;

                case "nuevaContrasena":
                    request.setAttribute("user", request.getParameter("user"));
                    if (!new UserHelper().modificarCliente(request)) {
                        redir = "WEB-INF/error.jsp";
                        request.setAttribute("mensaje", "Se ha producido un error. Puede ser que se hayan introducido parámetros incorrectos");
                    } else {
                        redir = "WEB-INF/exito.jsp";
                        request.setAttribute("mensaje", "Se ha cambiado la contraseña del usuario <b>" + request.getParameter("user") + "</b> con éxito");
                    }
                    break;

                case "nuevoCD":
                    redir = "WEB-INF/nuevoCD.jsp";
                    break;

                case "do_newcd":
                    new CDHelper().añadirCD(request);
                    redir = "WEB-INF/nuevoCD.jsp";
                    break;

                case "modificarStock":
                    new CDHelper().modificarCD(request);
                    redir = "WEB-INF/catalogoAdmin.jsp";
                    break;

                case "chpage_cat":
                    page = Integer.parseInt(request.getParameter("pag"));
                    if (u == null || !u.isAdmin()) {
                        redir = "WEB-INF/catalogo.jsp";
                    } else {
                        redir = "WEB-INF/catalogoAdmin.jsp";
                    }
                    break;

                case "chpage_usr":
                    page = Integer.parseInt(request.getParameter("pag"));
                    redir = "WEB-INF/listaClientes.jsp";
                    break;

                default:
                    page = 1;
                    if (u == null || !u.isAdmin()) {
                        redir = "WEB-INF/catalogo.jsp";
                    } else {
                        redir = "WEB-INF/admin.jsp";
                    }
                    break;
            }
        } else {
            page = 1;
            if (u == null || !u.isAdmin()) {
                redir = "WEB-INF/catalogo.jsp";
            } else {
                redir = "WEB-INF/admin.jsp";
            }

        }
        switch (redir) {
            case "WEB-INF/catalogoAdmin.jsp":
            case "WEB-INF/catalogo.jsp":
                CDPageHelper cd = new CDPageHelper();
                request.setAttribute("listaCDs", cd.getPage(page));
                request.setAttribute("totalPaginas", cd.getPagesTotal());
                break;
            case "WEB-INF/listaClientes.jsp":
                ClientesPageHelper clientes = new ClientesPageHelper();
                request.setAttribute("listaClientes", clientes.getPage(page));
                request.setAttribute("totalPaginas", clientes.getPagesTotal());
                break;
        }
        ses.setAttribute("page", page);
        request.getRequestDispatcher(redir).forward(request, response);

    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        procesador(request, response);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        procesador(request, response);
    }

}
