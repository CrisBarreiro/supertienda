package controlador;

import javax.servlet.http.HttpServletRequest;
import modelo.CD;
import modelo.ListaCD;
import modelo.DAOCD;

public class CDPageHelper {

    private final Integer pageSize = 12;

    public CDPageHelper() {

    }

    public ListaCD getPage(Integer page) {
        DAOCD dao = new DAOCD();
        page = checkPage(page);
        int inicio = (page - 1) * pageSize;
        ListaCD lista = dao.readList(inicio, pageSize);
        lista.setNumeroPagina(page);
        return lista;
    }

    public Integer getPagesTotal() {
        DAOCD dao = new DAOCD();
        int pagesTotal = (int) Math.ceil((double) dao.countCDs() / (double) pageSize);
        return pagesTotal;
    }

    public Integer getPagesTotal(HttpServletRequest req) {
        return getPagesTotal(genCD(req));
    }

    public Integer getPagesTotal(CD cd) {
        DAOCD dao = new DAOCD();
        int pagesTotal = (int) Math.ceil((double) dao.countSearch(cd) / (double) pageSize);
        return pagesTotal;
    }

    private int checkPage(int page) {
        if (page < 1) {
            return 1;
        } else {
            int k = getPagesTotal();
            if (page > k) {
                return k;
            }
        }
        return page;
    }

    private int checkPage(int page, CD cd) {
        if (page < 1) {
            return 1;
        } else {
            int k = getPagesTotal(cd);
            if (page > k) {
                return k;
            }
        }
        return page;
    }

    public CD genCD(HttpServletRequest req) {
        CD cd = new CD();
        if (req.getParameter("titulo") != null) {
            cd.setTitulo((String) req.getParameter("titulo"));
        }
        if (req.getParameter("autor") != null) {
            cd.setInterprete((String) req.getParameter("autor"));
        }
        if (req.getParameter("precio") != null) {
            String p = (String) req.getParameter("precio");
            if (!p.isEmpty()) {
                cd.setPrecio(Float.parseFloat(p));
            }
        }
        if (req.getParameter("pais") != null) {
            cd.setPais((String) req.getParameter("pais"));
        }
        return cd;
    }

    public ListaCD getSearchPage(HttpServletRequest req, Integer page) {
        DAOCD dao = new DAOCD();

        CD cd = genCD(req);

        page = checkPage(page, cd);
        int inicio = (page - 1) * pageSize;
        ListaCD readSearch = dao.readSearch(cd, inicio, pageSize);
        readSearch.setNumeroPagina(page);

        return readSearch;
    }
    
     public ListaCD getSearchPage(CD cd, Integer page) {
        DAOCD dao = new DAOCD();
       
        page = checkPage(page, cd);
        int inicio = (page - 1) * pageSize;
        ListaCD readSearch = dao.readSearch(cd, inicio, pageSize);
        readSearch.setNumeroPagina(page);

        return readSearch;
    }

}
