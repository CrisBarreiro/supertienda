package controlador;

import javax.servlet.http.HttpServletRequest;
import modelo.CD;
import modelo.DAOCD;

public class CDHelper {

    public CDHelper() {
    }

    public void añadirCD(HttpServletRequest request) {
        DAOCD dcd = new DAOCD();
        CD cd = new CD();
        cd.setInterprete((String) request.getParameter("interprete"));
        cd.setPais((String) request.getParameter("pais"));
        cd.setPrecio(Float.parseFloat((String) request.getParameter("precio")));
        cd.setTitulo((String) request.getParameter("titulo"));
        cd.setStock(Integer.parseInt((String) request.getParameter("stock")));
        dcd.nuevoCD(cd);
    }

    public void modificarCD(HttpServletRequest request) {
        DAOCD dao = new DAOCD();
        CD cd = new CD();
        cd.setCodigo(request.getParameter("cd"));
        dao.setStock(cd, Integer.parseInt(request.getParameter("stock")));
    }

}
