package controlador;

import modelo.ListaClientes;
import java.util.*;
import modelo.DAOUsuario;

public class ClientesPageHelper {

    private final Integer pageSize = 12;
    private Integer pageNumber;

    public ClientesPageHelper() {
    }

    public ListaClientes getPage(Integer page) {
        page=checkPage(page);
        DAOUsuario dao = new DAOUsuario();
        int inicio = (page - 1) * pageSize;
        ListaClientes lista = dao.readList(inicio, pageSize);
        lista.setNumeroPagina(page);
        return lista;
    }
    
      private int checkPage(int page) {
        if (page < 1) {
            return 1;
        } else {
            int k = getPagesTotal();
            if (page > k) {
                return k;
            }
        }
        return page;
    }


    public Integer getPagesTotal() {
        DAOUsuario dao = new DAOUsuario();
        int pagesTotal = (int) Math.ceil((double) dao.countUsuarios() / (double) pageSize);
        return pagesTotal;
    }

}
