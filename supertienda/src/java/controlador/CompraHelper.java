package controlador;

import java.util.ArrayList;
import java.util.Collections;
import modelo.Cliente;
import modelo.Carrito;
import javax.servlet.http.HttpServletRequest;
import modelo.CD;
import modelo.DAOCD;
import modelo.DAOCompra;
import modelo.DAOUsuario;
import modelo.MailDispatcher;

public class CompraHelper {

    public CompraHelper() {
    }

    public Cliente realizarCompra(Carrito carrito, Cliente cliente) {
        DAOCompra compra = new DAOCompra();

        compra.realizarCompra(carrito, cliente);
        MailDispatcher mailDispatcher = new MailDispatcher(cliente, carrito);
        mailDispatcher.start();
        actualizarVip(cliente);
        return cliente;
    }

    private void actualizarVip(Cliente cliente) {
        if (!cliente.isVip()) {
            DAOCompra dao = new DAOCompra();
            Float totalGastado = dao.totalGastado(cliente);
            boolean actualizar = cliente.actualizarVip(totalGastado);
            if (actualizar == true) {
                new DAOUsuario().updateVip(cliente);
            }
        }
    }

    public Carrito aplicarDescuento(Carrito carrito, Cliente cliente) {
        if (cliente.isVip()) {
            carrito.aplicarDescuento();
        }
        return carrito;
    }

    public Carrito añadirProducto(HttpServletRequest request, Carrito carrito) {

        if (request.getParameter("items") != null) {
            DAOCD cd = new DAOCD();
            for (String f : request.getParameterValues("items")) {
                carrito.añadirProducto(cd.readCd(f));
            }
        }
        return carrito;
    }

    public Carrito modificarCarrito(HttpServletRequest request, Carrito carrito) {
        ArrayList<String> params = Collections.list(request.getParameterNames());

        for (String a : params) {
            if (a.matches("^[0-9]{1,}$")) {
                CD cd = new CD();
                cd.setCodigo(a);
                carrito.cambiarCantidad(cd, Integer.parseInt(request.getParameter(a)));
            }

        }

        return carrito;
    }

}
