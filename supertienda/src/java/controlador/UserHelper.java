package controlador;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import modelo.Cliente;
import modelo.DAOUsuario;
import modelo.Usuario;

public class UserHelper {

    public UserHelper() {
    }

    public Usuario login(HttpServletRequest request) {
        String mail = request.getParameter("mail");
        String pass = request.getParameter("pass");
        Usuario usuarioRecibido = new Usuario(mail, pass);
        Usuario usuario = new DAOUsuario().comprobarLogin(usuarioRecibido);
        return usuario;
    }

    public boolean modificarCliente(HttpServletRequest request) {
        String pass = request.getParameter("pass");
        String pass2 = request.getParameter("pass2");
        if (!pass.equals(pass2)) {
            return false;
        } else {
            Cliente cliente = new Cliente();
            cliente.setMail(request.getParameter("user"));
            cliente.setPassword(pass);
            DAOUsuario dao = new DAOUsuario();
            dao.modificarUsuario(cliente);
            return true;
        }
    }

    public boolean registrar(HttpServletRequest request) {
        String email = request.getParameter("email");
        String pass = request.getParameter("pass");
        String repe = request.getParameter("repe");
        String nombre = request.getParameter("nombre");
        String apellidos = request.getParameter("apellidos");
        boolean resultado;

        if (email == null || email.isEmpty() || pass == null || pass.isEmpty() || repe == null || repe.isEmpty() || nombre == null
                || nombre.isEmpty() || apellidos == null || apellidos.isEmpty() || !repe.equals(pass)) {
            return false;
        } else {
            Cliente c = new Cliente(nombre, apellidos, email, pass);
            DAOUsuario usr = new DAOUsuario();
            try {
                if (usr.nuevoUsuario(c)){
                    resultado = true;
                } 
                else {
                    resultado = false;
                }
            } catch (SQLException ex) {
                return false;
            }
            return resultado;
        }
    }

    public void eliminarCliente(HttpServletRequest request) {
        DAOUsuario dao = new DAOUsuario();
        Cliente cliente = new Cliente();
        cliente.setMail(request.getParameter("user"));
        dao.delete((cliente));
    }
}
