package modelo;

import java.util.*;

public class Cliente extends Usuario {

    public Cliente() {
    }

    public Cliente(String nombre, String apellidos, String mail, String password) {
        super();
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.mail = mail;
        this.password = password;
        this.vip = false;
    }

    public Cliente(String nombre, String apellidos, String mail, String password, boolean vip) {
        super();
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.mail = mail;
        this.password = password;
        this.vip = vip;
    }

    private boolean vip;
    private static final Float vip_min = (float) 100.00;

    public boolean actualizarVip(Float gastos) {
        if (gastos > vip_min) {
            this.vip = true;
            return true;
        }
        return false;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getMail() {
        return mail;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setVip(boolean vip) {
        this.vip = vip;
    }

    public boolean isVip() {
        return vip;
    }

}
