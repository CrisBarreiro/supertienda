package modelo;

import java.util.*;

public class Carrito {

    public Carrito() {
        this.precioTotal = (float) 0;
        this.lineasCompra = new ArrayList<>();
        this.descuento = (float) 0;

    }

    private Float precioTotal;
    private List<LineaCompra> lineasCompra;
    private Float descuento;
    private static final Float fraccion_desc = (float) 0.2;

    public void aplicarDescuento() {
        this.descuento = fraccion_desc * precioTotal;
    }

    public void añadirProducto(CD k) {
        boolean repe = false;
        for (LineaCompra lc : this.lineasCompra) {
            if (lc.getCd().equals(k)) {
                repe = true;
                if (lc.getCantidad() < lc.getCd().getStock()) {
                    lc.setCantidad(lc.getCantidad() + 1);
                }
            }
        }
        if (repe == false) {
            this.lineasCompra.add(new LineaCompra(k, 1));
        }
        calcularPrecioTotal();
    }

    public void cambiarCantidad(CD k, int cantidad) {
        if (cantidad == 0) {
            this.eliminarProducto(k);
        } else {
            for (LineaCompra a : lineasCompra) {
                if (a.getCd().equals(k) && cantidad <= a.getCd().getStock()) {
                    a.setCantidad(cantidad);
                }
            }
        }
        calcularPrecioTotal();
    }

    private void eliminarProducto(CD k) {
        LineaCompra eliminar = null;
        for (LineaCompra a : lineasCompra) {
            if (a.getCd().equals(k)) {
                eliminar = a;
            }
        }
        lineasCompra.remove(eliminar);
        calcularPrecioTotal();
    }

    private void calcularPrecioTotal() {
        Float k = (float) 0.0;
        for (LineaCompra l : lineasCompra) {
            k += (float) l.getCd().getPrecio() * (float) l.getCantidad();
        }
        this.precioTotal = k;
    }

    public Float getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(Float precioTotal) {
        this.precioTotal = precioTotal;
    }

    public List<LineaCompra> getLineasCompra() {
        return lineasCompra;
    }

    public void setLineasCompra(List<LineaCompra> lineasCompra) {
        this.lineasCompra = lineasCompra;
    }

    public Float getDescuento() {
        return descuento;
    }

    public void setDescuento(Float descuento) {
        this.descuento = descuento;
    }

}
