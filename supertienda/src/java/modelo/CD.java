package modelo;

import java.util.*;

public class CD {

    public CD() {
        titulo = "";
        interprete = "";
        pais = "";
        precio = (float) -1.0;
        stock = -1;
        codigo = "";
    }

    private String titulo;
    private String interprete;
    private String pais;
    private Float precio;
    private Integer stock;
    private String codigo;

    public String getTitulo() {
        return titulo;
    }

    public CD(String titulo, String interprete, String pais, Float precio, Integer stock, String codigo) {
        this.titulo = titulo;
        this.interprete = interprete;
        this.pais = pais;
        this.precio = precio;
        this.stock = stock;
        this.codigo = codigo;
    }

    public CD(String titulo, String interprete, String pais, Float precio, String codigo) {
        this.titulo = titulo;
        this.interprete = interprete;
        this.pais = pais;
        this.stock = -1;
        this.precio = precio;
        this.codigo = codigo;
    }

    public CD(String titulo, String interprete, String pais, Float precio, Integer stock) {
        this.titulo = titulo;
        this.interprete = interprete;
        this.pais = pais;
        this.stock = -1;
        this.precio = precio;
        this.codigo = "";
        this.stock = stock;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getInterprete() {
        return interprete;
    }

    public void setInterprete(String interprete) {
        this.interprete = interprete;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Float getPrecio() {
        return precio;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.codigo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CD other = (CD) obj;
        if (!Objects.equals(this.codigo, other.codigo)) {
            return false;
        }
        return true;
    }

}
