package modelo;

import java.util.*;

public class LineaCompra {

    public LineaCompra() {
        this.cd = new CD();
        this.cantidad = 0;
    }

    public LineaCompra(CD cd, Integer cantidad) {
        this.cd = cd;
        this.cantidad = cantidad;
    }

    private CD cd;
    private Integer cantidad;

    public CD getCd() {
        return cd;
    }

    public void setCd(CD cd) {
        this.cd = cd;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

}
