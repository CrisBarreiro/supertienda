package modelo;

import java.util.*;

public class ListaCD {

    public ListaCD() {
    }

    private List<CD> CDs;
    private Integer numeroPagina;

    public List<CD> getCDs() {
        return CDs;
    }

    public void setCDs(List<CD> CDs) {
        this.CDs = CDs;
    }

    public Integer getNumeroPagina() {
        return numeroPagina;
    }

    public void setNumeroPagina(Integer numeroPagina) {
        this.numeroPagina = numeroPagina;
    }

}
