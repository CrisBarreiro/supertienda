package modelo;

import java.util.*;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.nocrala.tools.texttablefmt.Table;

public class MailDispatcher extends Thread {

    public MailDispatcher(Cliente cliente, Carrito carrito) {
        cli = cliente;
        car = carrito;
    }

    private static final String from = "tiendasuper2@gmail.com";
    private static final String server = "smtp.googlemail.com";
    private static final String pass = "supertienda1234";
    private final Carrito car;
    private final Cliente cli;

    @Override
    public void run() {
        this.sendMail(cli, car);
    }

    private class SMTPAuthenticator extends javax.mail.Authenticator {

        @Override
        public PasswordAuthentication getPasswordAuthentication() {
            String username = from;           // specify your email id here (sender's email id)
            String password = pass;                                      // specify your password here
            return new PasswordAuthentication(username, password);
        }
    }

    public void sendMail(Cliente cliente, Carrito carrito) {
        String to = cliente.getMail();

        Properties properties = System.getProperties();

        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", server);
        properties.put("mail.smtp.auth", "true");
        Authenticator auth = new SMTPAuthenticator();

        Session session = Session.getInstance(properties, auth);

        Float precioF = carrito.getPrecioTotal() - carrito.getDescuento();

        String mensaje = "Estimado " + cliente.getNombre() + " " + cliente.getApellidos() + ":\n"
                + "Gracias por su compra valorada en " + String.format("%.2f", precioF) + "€\n"
                + "\n"
                + " Artículos comprados:\n";
        Table t = new Table(5);
        t.addCell("Artista");
        t.addCell("Album");
        t.addCell("PVP");
        t.addCell("Unidades");
        t.addCell("Precio");
        System.out.println(t.render());
        for (LineaCompra l : carrito.getLineasCompra()) {
            CD cd = l.getCd();
            Float ptotal = cd.getPrecio() * (float) l.getCantidad();
            t.addCell(cd.getInterprete());
            t.addCell(cd.getTitulo());
            t.addCell(String.format("%.2f", cd.getPrecio()) + " €");
            t.addCell(l.getCantidad().toString());
            t.addCell(String.format("%.2f", ptotal) + " €");
        }
        mensaje = mensaje + t.render() + "\n\n";
        t = new Table(2);
        t.addCell("Precio final");
        t.addCell(String.format("%.2f", precioF) + " €");
        t.addCell("Precio total");
        t.addCell(String.format("%.2f", carrito.getPrecioTotal()) + " €");
        t.addCell("Descuento");
        t.addCell(String.format("%.2f", carrito.getDescuento()) + " €");
        mensaje = mensaje + t.render();
        mensaje += "\n\nGracias por su compra!";

        try {
            MimeMessage message = new MimeMessage(session);

            message.setFrom(new InternetAddress(from));

            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            message.setSubject("Gracias por tu compra");

            message.setText(mensaje);

            Transport.send(message);
            System.out.println("message sent to " + to);
        } catch (MessagingException mex) {
            System.out.println(mex.getMessage());
        }
    }
}
