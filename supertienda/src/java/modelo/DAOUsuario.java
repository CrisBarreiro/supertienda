package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOUsuario {

    public DAOUsuario() {
    }

    public synchronized boolean nuevoUsuario(Cliente cliente) throws SQLException {
        Conector conector = new Conector();
        Connection conexion = conector.getConexion();
        PreparedStatement consulta = null;
        ResultSet resultado = null;
        boolean res = false;
        try {
            consulta = conexion.prepareStatement("SELECT * FROM admin WHERE mail LIKE ?");
            consulta.setString(1, cliente.getMail());
            resultado = consulta.executeQuery();
            if (!resultado.next()) {
                consulta = conexion.prepareStatement("INSERT INTO clientes VALUES (?, ?, ?, ?, ?)");
                consulta.setString(1, cliente.getNombre());
                consulta.setString(2, cliente.getApellidos());
                consulta.setString(3, cliente.getMail());
                consulta.setString(4, cliente.getPassword());
                consulta.setBoolean(5, cliente.isVip());
                consulta.execute();
                res = true;
            }
            else{
                res = false;
            }
        } catch (SQLException ex) {
            throw ex;
        } finally {

            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                }
                if (consulta != null && !consulta.isClosed()) {
                    consulta.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DAOUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return res;
    }

    public synchronized ListaClientes readList(Integer inicio, Integer paso) {
        Conector conector = new Conector();
        Connection conexion = conector.getConexion();
        ListaClientes listaClientes = new ListaClientes();
        ResultSet resultado = null;
        List<Cliente> listaResultados = new ArrayList();
        PreparedStatement consulta = null;
        try {
            consulta = conexion.prepareStatement("SELECT * FROM clientes ORDER BY mail LIMIT ?, ?");
            consulta.setInt(1, inicio);
            consulta.setInt(2, paso);
            resultado = consulta.executeQuery();
            while (resultado.next()) {
                String nombre = resultado.getString("nombre");
                String apellidos = resultado.getString("apellidos");
                String mail = resultado.getString("mail");
                String pass = resultado.getString("password");
                boolean vip = resultado.getBoolean("vip");
                listaResultados.add(new Cliente(nombre, apellidos, mail, pass, vip));
            }
            listaClientes.setClientes(listaResultados);
        } catch (SQLException ex) {
            ex.getMessage();
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                }
                if (consulta != null && !consulta.isClosed()) {
                    consulta.close();
                }
                if (resultado != null && !resultado.isClosed()) {
                    resultado.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DAOUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listaClientes;
    }

    public synchronized void updateVip(Cliente cliente) {
        Conector conector = new Conector();
        Connection conexion = conector.getConexion();
        PreparedStatement consulta = null;
        try {
            consulta = conexion.prepareStatement("UPDATE clientes SET vip = ? WHERE mail = ?");
            consulta.setBoolean(1, cliente.isVip());
            consulta.setString(2, cliente.getMail());
            consulta.execute();
        } catch (SQLException ex) {
            Logger.getLogger(DAOUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public synchronized Usuario comprobarLogin(Usuario usuario) {
        Conector conector = new Conector();
        Connection conexion = conector.getConexion();
        PreparedStatement consulta = null;
        ResultSet resultado = null;
        Cliente cliente = null;
        Admin admin = null;
        try {
            consulta = conexion.prepareStatement("SELECT * FROM clientes WHERE mail LIKE ? AND password LIKE ?");
            consulta.setString(1, usuario.getMail());
            consulta.setString(2, usuario.getPassword());
            resultado = consulta.executeQuery();
            if (resultado.next()) {
                if (resultado != null) {
                    String nombre = resultado.getString("nombre");
                    String apellidos = resultado.getString("apellidos");
                    String mail = resultado.getString("mail");
                    String password = resultado.getString("password");
                    Boolean vip = resultado.getBoolean("vip");
                    cliente = new Cliente(nombre, apellidos, mail, password, vip);
                }
            }
            if (cliente == null) {
                consulta = conexion.prepareStatement("SELECT * FROM admin WHERE mail LIKE ? AND password LIKE ?");
                consulta.setString(1, usuario.getMail());
                consulta.setString(2, usuario.getPassword());
                resultado = consulta.executeQuery();
                if (resultado.next()) {
                    if (resultado != null) {
                        String nombre = resultado.getString("nombre");
                        String apellidos = resultado.getString("apellidos");
                        String mail = resultado.getString("mail");
                        String password = resultado.getString("password");
                        admin = new Admin(nombre, apellidos, mail, password);

                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {

            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                }
                if (consulta != null && !consulta.isClosed()) {
                    consulta.close();
                }
                if (resultado != null && !resultado.isClosed()) {
                    resultado.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DAOUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (cliente != null) {
            return cliente;
        } else if (admin != null) {
            return admin;
        } else {
            return null;
        }
    }

    public synchronized void delete(Cliente cliente) {
        Connection conexion = new Conector().getConexion();
        PreparedStatement consulta = null;
        try {
            consulta = conexion.prepareStatement("DELETE FROM clientes WHERE mail LIKE ?");
            consulta.setString(1, cliente.getMail());
            consulta.execute();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                }
                if (consulta != null && !consulta.isClosed()) {
                    consulta.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DAOUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public synchronized void modificarUsuario(Cliente cliente) {
        Connection conexion = new Conector().getConexion();
        PreparedStatement consulta = null;
        try {
            consulta = conexion.prepareStatement("UPDATE clientes SET password = ? WHERE mail LIKE ?");
            consulta.setString(1, cliente.getPassword());
            consulta.setString(2, cliente.getMail());
            consulta.execute();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {

            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                }
                if (consulta != null && !consulta.isClosed()) {
                    consulta.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DAOUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public synchronized int countUsuarios() {
        Connection conexion = new Conector().getConexion();
        ResultSet resultado = null;
        int result = 0;
        PreparedStatement consulta = null;

        try {
            consulta = conexion.prepareStatement("SELECT count(*) as total FROM clientes");
            resultado = consulta.executeQuery();
            if (resultado.next()) {
                result = resultado.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {

            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                }
                if (consulta != null && !consulta.isClosed()) {
                    consulta.close();
                }
                if (resultado != null && !resultado.isClosed()) {
                    resultado.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DAOUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
}
