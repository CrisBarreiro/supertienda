package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOCD {

    public DAOCD() {
    }

    public synchronized void nuevoCD(CD cd) {
        Conector conector = new Conector();
        Connection conexion = conector.getConexion();
        String codigo = "";
        PreparedStatement consulta = null;
        ResultSet resultado = null;

        try {
            conexion.setAutoCommit(false);
            consulta = conexion.prepareStatement("INSERT INTO cd (titulo, interprete, pais, precio) VALUES (?, ?, ?, ?)");
            consulta.setString(1, cd.getTitulo());
            consulta.setString(2, cd.getInterprete());
            consulta.setString(3, cd.getPais());
            consulta.setFloat(4, cd.getPrecio());
            consulta.execute();
            consulta = conexion.prepareStatement("SELECT codigo FROM cd ORDER BY codigo desc LIMIT 1");
            resultado = consulta.executeQuery();
            while (resultado.next()) {
                codigo = resultado.getString("codigo");
            }
            consulta = conexion.prepareStatement("INSERT INTO stock VALUES (?, ?)");
            consulta.setString(1, codigo);
            consulta.setInt(2, cd.getStock());
            consulta.execute();
            conexion.commit();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {

            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                }
                if (consulta != null && !consulta.isClosed()) {
                    consulta.close();
                }
                if (resultado != null && !resultado.isClosed()) {
                    resultado.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DAOCD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public synchronized ListaCD readList(Integer inicio, Integer paso) {
        Conector conector = new Conector();
        Connection conexion = conector.getConexion();
        ListaCD listaCD = new ListaCD();
        ResultSet resultado = null;
        List<CD> listaResultados = new ArrayList();
        PreparedStatement consulta = null;
        try {
            consulta = conexion.prepareStatement("SELECT * FROM cd INNER JOIN stock ON cd.codigo=stock.cd LIMIT ?, ?");
            consulta.setInt(1, inicio);
            consulta.setInt(2, paso);
            resultado = consulta.executeQuery();
            while (resultado.next()) {
                listaResultados.add(new CD(resultado.getString("titulo"), resultado.getString("interprete"), resultado.getString("pais"), resultado.getFloat("precio"), resultado.getInt("stock"), resultado.getString("codigo")));
            }
            listaCD.setCDs(listaResultados);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {

            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                }
                if (consulta != null && !consulta.isClosed()) {
                    consulta.close();
                }
                if (resultado != null && !resultado.isClosed()) {
                    resultado.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DAOCD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return listaCD;
    }

    public synchronized ListaCD readSearch(CD cd, Integer inicio, Integer paso) {
        Conector conector = new Conector();
        Connection conexion = conector.getConexion();
        ListaCD listaCD = new ListaCD();
        ResultSet resultado = null;
        List<CD> listaResultados = new ArrayList();
        PreparedStatement consulta = null;

        try {
            consulta = conexion.prepareStatement("SELECT * FROM cd INNER JOIN stock ON cd.codigo=stock.cd WHERE titulo LIKE ? AND interprete LIKE ? AND pais LIKE ? AND precio BETWEEN 0 AND ? LIMIT ?, ?");
            consulta.setString(1, cd.getTitulo().isEmpty() ? "%" : "%" + cd.getTitulo() + "%");
            consulta.setString(2, cd.getInterprete().isEmpty() ? "%" : "%" + cd.getInterprete() + "%");
            consulta.setString(3, cd.getPais().isEmpty() ? "%" : "%" + cd.getPais() + "%");
            consulta.setFloat(4, cd.getPrecio() > 0 ? cd.getPrecio() : Float.MAX_VALUE);
            consulta.setInt(5, inicio);
            consulta.setInt(6, paso);
            resultado = consulta.executeQuery();
            while (resultado.next()) {
                listaResultados.add(new CD(resultado.getString("titulo"), resultado.getString("interprete"), resultado.getString("pais"), resultado.getFloat("precio"), resultado.getInt("stock"), resultado.getString("codigo")));
            }
            listaCD.setCDs(listaResultados);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {

            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                }
                if (consulta != null && !consulta.isClosed()) {
                    consulta.close();
                }
                if (resultado != null && !resultado.isClosed()) {
                    resultado.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DAOCD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listaCD;
    }

    public synchronized void setStock(CD cd, Integer stock) {
        Conector conector = new Conector();
        Connection conexion = conector.getConexion();
        PreparedStatement consulta = null;
        try {
            consulta = conexion.prepareStatement("UPDATE stock SET stock = ? WHERE cd LIKE ?");
            consulta.setInt(1, stock);
            consulta.setString(2, cd.getCodigo());
            consulta.execute();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {

            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                }
                if (consulta != null && !consulta.isClosed()) {
                    consulta.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DAOCD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public synchronized void restarStock(CD cd, Integer stock) {
        Conector conector = new Conector();
        Connection conexion = conector.getConexion();
        PreparedStatement consulta = null;
        try {
            consulta = conexion.prepareStatement("UPDATE stock SET stock = stock - ? WHERE cd LIKE ?");
            consulta.setInt(1, stock);
            consulta.setString(2, cd.getCodigo());
            consulta.execute();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {

            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                }
                if (consulta != null && !consulta.isClosed()) {
                    consulta.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(DAOCD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public synchronized CD readCd(String f) {
        Connection conexion = new Conector().getConexion();
        ResultSet resultado = null;
        CD result = null;
        PreparedStatement consulta = null;

        try {
            consulta = conexion.prepareStatement("SELECT * FROM cd INNER JOIN stock ON cd.codigo=stock.cd WHERE codigo=?");
            consulta.setString(1, f);
            resultado = consulta.executeQuery();
            if (resultado.next()) {
                result = new CD(resultado.getString("titulo"), resultado.getString("interprete"), resultado.getString("pais"), resultado.getFloat("precio"), resultado.getInt("stock"), resultado.getString("codigo"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {

            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                }
                if (consulta != null && !consulta.isClosed()) {
                    consulta.close();
                }
                if (resultado != null && !resultado.isClosed()) {
                    resultado.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DAOCD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return result;
    }

    public synchronized int countCDs() {
        Connection conexion = new Conector().getConexion();
        ResultSet resultado = null;
        int result = 0;
        PreparedStatement consulta = null;

        try {
            consulta = conexion.prepareStatement("SELECT count(*) as total FROM cd");
            resultado = consulta.executeQuery();
            if (resultado.next()) {
                result = resultado.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {

            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                }
                if (consulta != null && !consulta.isClosed()) {
                    consulta.close();
                }
                if (resultado != null && !resultado.isClosed()) {
                    resultado.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DAOCD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return result;
    }

    public synchronized int countSearch(CD cd) {
        Connection conexion = new Conector().getConexion();
        ResultSet resultado = null;
        int result = 0;
        PreparedStatement consulta = null;

        try {
            consulta = conexion.prepareStatement("SELECT count(*) FROM cd  WHERE titulo LIKE ? AND interprete LIKE ? AND pais LIKE ? AND precio BETWEEN 0 AND ?");
            consulta.setString(1, cd.getTitulo().isEmpty() ? "%" : "%" + cd.getTitulo() + "%");
            consulta.setString(2, cd.getInterprete().isEmpty() ? "%" : "%" + cd.getInterprete() + "%");
            consulta.setString(3, cd.getPais().isEmpty() ? "%" : "%" + cd.getPais() + "%");
            consulta.setFloat(4, cd.getPrecio() > 0 ? cd.getPrecio() : Float.MAX_VALUE);

            resultado = consulta.executeQuery();
            if (resultado.next()) {
                result = resultado.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {

            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                }
                if (consulta != null && !consulta.isClosed()) {
                    consulta.close();
                }
                if (resultado != null && !resultado.isClosed()) {
                    resultado.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DAOCD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return result;
    }
}
