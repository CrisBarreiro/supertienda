package modelo;

import java.util.*;

public class Usuario {

    public Usuario() {
    }

    public Usuario(String mail, String password) {
        this.nombre = "";
        this.apellidos = "";
        this.mail = mail;
        this.password = password;

    }
    protected String nombre;
    protected String apellidos;
    protected String mail;
    protected String password;

    public boolean isAdmin() {

        return this instanceof Admin;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getMail() {
        return mail;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
