package modelo;

import java.util.*;

public class Admin extends Usuario {

    public Admin() {
    }

    Admin(String nombre, String apellidos, String mail, String password) {
        super();
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.mail = mail;
        this.password = password;
    }

}
