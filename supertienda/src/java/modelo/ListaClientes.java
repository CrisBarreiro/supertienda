package modelo;

import java.util.*;

public class ListaClientes {

    public ListaClientes() {
    }

    private List<Cliente> clientes;
    private Integer numeroPagina;

    public List<Cliente> getClientes() {
        return clientes;
    }

    public Integer getNumeroPagina() {
        return numeroPagina;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    public void setNumeroPagina(Integer numeroPagina) {
        this.numeroPagina = numeroPagina;
    }
}
