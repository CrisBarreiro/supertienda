# superTienda repo #

Repositorio colaborativo para un proyecto de tienda virtual realizado junto con Álvaro Brey para la asignatura de Diseño de Aplicaciones Web Avanzadas de 3º curso de Grado en Ingeniería Informática.

La rama Web-services utiliza servicios web extraídos del repositorio CrisBarreiro/Web-Services/supertienda_superservicios

**Tecnologías empleadas**

* Java
* JSP
* Servicios web (ReST y SOAP)
* MySQL

Servidores de aplicaciones donde se ha desplegado: Glassfish

La documentación de este proyecto se encuentra en el directorio doc, en ella pueden encontrarse un diagrama de clases utilizando la notación UML y los scripts de creación de la base de datos